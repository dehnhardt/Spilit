BUNDLE = mkl-spilit.lv2
INSTALL_DIR = /usr/local/lib/lv2


$(BUNDLE): manifest.ttl mkl-spilit.ttl src/mkl-spilit.so
	rm -rf $(BUNDLE)
	mkdir $(BUNDLE)
	cp manifest.ttl mkl-spilit.ttl src/mkl-spilit.so $(BUNDLE)

src/mkl-spilit.so: src/mkl-spilit.cpp
	g++ -shared -fPIC -DPIC src/mkl-spilit.cpp `pkg-config --cflags --libs lv2-plugin` -o src/mkl-spilit.so

src/mkl-spilit.peg: mkl-spilit.ttl src/mkl-spilit.peg
	lv2peg mkl-spilit.ttl src/mkl-spilit.peg

install: $(BUNDLE)
	if [ -d "/usr/lib/lv2" ]; then echo "Dir INSTALL_DIR=/usr/lib exists"; INSTALL_DIR=/usr/lib/lv2; fi
	if [ -d "/usr/local/lib/lv2" ]; then echo "Dir /usr/local/lib exists"; INSTALL_DIR=/usr/local/lib/lv2; fi
	mkdir -p $(INSTALL_DIR)
	rm -rf $(INSTALL_DIR)/$(BUNDLE)
	cp -R $(BUNDLE) $(INSTALL_DIR)

clean:
	rm -rf $(BUNDLE) src/mkl-spilit.so rc/mkl-spilit.peg
