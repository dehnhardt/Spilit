#include "lv2/core/lv2.h"
#include <cmath>
#include <stdlib.h>
#include <iostream>

#include "mkl-spilit.h"
#include "mkl-spilit.peg"

#define SPILIT_URI "http://punkt-k.de/plugins/mkl-spilit"

void Spilit::connectPort( const uint32_t port, void* data )
{
  switch( port )
  {
    case mkl_spilit_outselect:
      output_select = (float*)data;
      break;
    case mkl_spilit_in_l:
      input_l = (float*)data;
      break;
    case mkl_spilit_in_r:
      input_r = (float*)data;
      break;
     case mkl_spilit_out_a_l:
      output_a_l = (float*)data;
      break;
    case mkl_spilit_out_a_r:
      output_a_r = (float*)data;
      break;
    case mkl_spilit_out_b_l:
      output_b_l = (float*)data;
      break;
    case mkl_spilit_out_b_r:
      output_b_r = (float*)data;
      break;
  }
}

void Spilit::run (const uint32_t sample_count) 
{
  for (uint32_t pos = 0; pos < sample_count; pos++)
  {
    const int outports = static_cast< int >( *output_select );

    if( ( outports & 1 ) == 1 ){
        output_a_l[pos] = input_l[pos];
        output_a_r[pos] = input_r[pos];
    }
    if( ( outports & 2 ) == 2 ){
        output_b_l[pos] = input_l[pos];
        output_b_r[pos] = input_r[pos];
    }
  }
}

static LV2_Handle
instantiate(const LV2_Descriptor*     descriptor,
            double                    rate,
            const char*               bundle_path,
            const LV2_Feature* const* features)
{

    Spilit* spilit = nullptr;
    try 
    {
        spilit = new Spilit ();
    } 
    catch (const std::bad_alloc& ba)
    {
        std::cerr << "Failed to allocate memory. Can't instantiate MKL-Spilit" << std::endl;
        return nullptr;
    }
    return spilit;
}

// LV2 Methods

static void
connect_port(LV2_Handle instance, uint32_t port, void* data)
{
  Spilit* spilit = static_cast< Spilit* >( instance );
  if( spilit != nullptr) 
    spilit->connectPort( port, data );
}


static void
activate(LV2_Handle instance)
{}


static void
run(LV2_Handle instance, uint32_t n_samples)
{
  Spilit* spilit = static_cast< Spilit* >( instance );
  if( spilit != nullptr) 
    spilit->run( n_samples );

}

static void
deactivate(LV2_Handle instance)
{}

static void
cleanup(LV2_Handle instance)
{
  Spilit* spilit = static_cast< Spilit* >( instance );
  if( spilit != nullptr) 
    delete spilit;
}

static const void*
extension_data(const char* uri)
{
  return NULL;
}

static const LV2_Descriptor descriptor = {SPILIT_URI,
                                          instantiate,
                                          connect_port,
                                          activate,
                                          run,
                                          deactivate,
                                          cleanup,
                                          extension_data };

LV2_SYMBOL_EXPORT
const LV2_Descriptor*
lv2_descriptor(uint32_t index)
{
  return index == 0 ? &descriptor : NULL;
}