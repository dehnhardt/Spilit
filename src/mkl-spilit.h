#pragma once

#include <cstdint>

class Spilit
{
    public:

        Spilit() = default;
        ~Spilit() = default;
        void run (const uint32_t sample_count);
        void connectPort (const uint32_t port, void* data);

    private: 
        float* input_l;
        float* input_r;
        float* output_a_l;
        float* output_a_r;
        float* output_b_l;
        float* output_b_r;
        float *output_select;
};
